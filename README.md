# Issue Tracker - REST API

Project represents REST API server for an issue tracker application.

It is developed as a demonstration for my thesis at the Faculty of Technical Sciencies in Novi Sad, Serbia 2018.

## How to run

To run the server this requirements should be met:
- Java 1.8 installed (tried with the version 1.8.0_181)
- Maven installed (tried with the version 3.5.2)
- TCP port 8090 should be free

Command to run server:
```bash
mvn clean spring-boot:run

```

## How to test REST API server

In doc folder there is curl.md file which contains curl commands for using REST API. For more comfortable usage please see front end part of issue tracker application.

## About me

My name is Igor Gere. During my early age I developed interest in programming which from then has just evolved in to interest of computer science. 

While attending Faculty of Technical Sciencies I got interested in web based applications. With over 10 years of professional development experience in the field of web applications I consider myself a true full stack developer.
