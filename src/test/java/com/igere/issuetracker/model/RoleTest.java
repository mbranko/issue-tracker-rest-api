package com.igere.issuetracker.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class RoleTest {
    @Test
    public void getAuthoritiesReturnsLowerAuthorities() {
        Assertions.assertThat(Role.ADMIN.getAuthorities().toArray())
                .containsOnly(Role.ADMIN, Role.USER, Role.OBSERVER);

        Assertions.assertThat(Role.USER.getAuthorities().toArray())
                .containsOnly(Role.USER, Role.OBSERVER);

        Assertions.assertThat(Role.OBSERVER.getAuthorities().toArray())
                .containsOnly(Role.OBSERVER);
    }
}