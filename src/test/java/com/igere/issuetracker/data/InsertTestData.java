package com.igere.issuetracker.data;

import com.igere.issuetracker.model.*;
import com.igere.issuetracker.repository.CommentRepository;
import com.igere.issuetracker.repository.IssueRepository;
import com.igere.issuetracker.repository.ProjectRepository;
import com.igere.issuetracker.repository.UserRepository;
import com.igere.issuetracker.security.AuthenticationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Collections;

@Component
public class InsertTestData {
    @Autowired
    UserRepository userRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    IssueRepository issueRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CommentRepository commentRepository;

    @EventListener(classes = ContextRefreshedEvent.class)
    @Transactional
    public void onContextStartedEvent() {
        User admin = new User("admin", passwordEncoder.encode("admin"), Role.ADMIN, true, Collections.emptySet());
        User user = new User("user", passwordEncoder.encode("user"), Role.USER, true, Collections.emptySet());
        User observer = new User("observer", passwordEncoder.encode("observer"), Role.OBSERVER, true, Collections.emptySet());

        AuthenticationUtil.setAuthentication(admin);

        admin = userRepository.save(admin);

        AuthenticationUtil.setAuthentication(admin);

        user = userRepository.save(user);
        userRepository.save(observer);

        Project project = projectRepository.save(new Project(
                "Issue Tracker",
                "This is my thesis demonstration project. Aim is to demonstrate how easily REST web services can be created.",
                Collections.emptyList()));

        Issue issue = issueRepository.save(Issue.builder().title("The first issue").description("This is the first issue in issue tracker.").priority(1L).project(project).type(Type.NOTE).state(State.OPEN).build());
        issueRepository.save(Issue.builder().title("The second issue").description("Lorem ipsum dolor sit amet").priority(1L).project(project).type(Type.NOTE).state(State.OPEN).build());
        issueRepository.save(Issue.builder().title("The third issue").description("Lorem ipsum dolor sit amet").priority(1L).project(project).type(Type.NOTE).state(State.OPEN).build());

        commentRepository.save(new Comment("Comment of Admin.", issue));

        AuthenticationUtil.setAuthentication(user);

        commentRepository.save(new Comment("Comment of User.", issue));

        AuthenticationUtil.clearAuthentication();
    }
}
