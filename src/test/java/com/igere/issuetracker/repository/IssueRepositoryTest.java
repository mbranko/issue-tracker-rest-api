package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Issue;
import com.igere.issuetracker.model.Role;
import com.igere.issuetracker.model.State;
import com.igere.issuetracker.model.Type;
import com.igere.issuetracker.security.AuthenticationUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IssueRepositoryTest {
    @Autowired
    IssueRepository issueRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Test
    public void anyoneCanFind() {
        issueRepository.findAll();
    }

    @Test
    @WithUserDetails
    public void userCanSave() {
        assert(AuthenticationUtil.getAuthorities().contains(Role.USER));
        save();
    }

    @Test(expected = AccessDeniedException.class)
    @WithUserDetails("observer")
    public void observerCanNotSave() {
        assert(AuthenticationUtil.getAuthorities().contains(Role.OBSERVER));
        save();
    }

    private void save() {
        issueRepository.save(Issue.builder()
                .title("Test issue")
                .description("Test issue.")
                .priority(1L)
                .project(projectRepository.findAll().iterator().next())
                .type(Type.NOTE).state(State.OPEN).build());
    }
}
