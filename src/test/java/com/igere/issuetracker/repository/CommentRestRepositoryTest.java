package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Role;
import com.igere.issuetracker.security.AuthenticationUtil;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CommentRestRepositoryTest {
    @Autowired
    MockMvc mvc;

    @Test
    public void authorizatioRequired() throws Exception {
        mvc.perform(get("/api/comments"))
                .andExpect(status().isUnauthorized());

        mvc.perform(get("/api/comments/9"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("observer")
    public void observerCanGet() throws Exception {
        assert(AuthenticationUtil.getAuthorities().contains(Role.OBSERVER));

        mvc.perform(get("/api/comments"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("@._embedded.comments", Matchers.hasSize(2)));
    }

    @Test
    @WithUserDetails
    public void creatorCanEdit() throws Exception {
        mvc.perform(patch("/api/comments/9").content("{\"content\":\"Creator tries to change comment.\"}"))
                .andExpect(status().isNoContent());

        mvc.perform(get("/api/comments/9"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("@.content", Matchers.is("Creator tries to change comment.")));
    }

    @Test
    @WithUserDetails("admin")
    public void nonCreatorCanNotEdit() throws Exception {
        mvc.perform(patch("/api/comments/9").content("{\"content\":\"Non creator tries to change comment.\"}"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("admin")
    public void nonCreatorCanNotDelete() throws Exception {
        mvc.perform(delete("/api/comments/9"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("admin")
    public void creatorCanDelete() throws Exception {
        mvc.perform(delete("/api/comments/8"))
                .andExpect(status().isNoContent());
    }
}
