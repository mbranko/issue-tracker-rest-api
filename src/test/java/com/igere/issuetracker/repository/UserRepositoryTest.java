package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Role;
import com.igere.issuetracker.model.User;
import com.igere.issuetracker.security.AuthenticationUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Test
    public void anyoneCanFind() {
        userRepository.findAll();
    }

    @Test
    @WithUserDetails("admin")
    public void adminCanSave() {
        assert(AuthenticationUtil.getAuthorities().contains(Role.ADMIN));
        save();
    }

    @Test(expected = AccessDeniedException.class)
    @WithUserDetails
    public void userCanNotSave() {
        assert(AuthenticationUtil.getAuthorities().contains(Role.USER));
        save();
    }

    @Test(expected = AccessDeniedException.class)
    @WithUserDetails("observer")
    public void observerCanNotSave() {
        assert(AuthenticationUtil.getAuthorities().contains(Role.OBSERVER));
        save();
    }

    private void save() {
        userRepository.save(new User("userCreatedByTest", "plain pass", Role.OBSERVER, true, Collections.emptySet()));
    }
}
