package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Role;
import com.igere.issuetracker.model.User;
import com.igere.issuetracker.security.AuthenticationUtil;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserRestRepositoryTest {
    @Autowired
    MockMvc mvc;

    @Autowired
    UserRepository userRepository;

    @Test
    public void authorizatioRequired() throws Exception {
        mvc.perform(get("/api/users"))
                .andExpect(status().isUnauthorized());

        mvc.perform(get("/api/users/1"))
                .andExpect(status().isUnauthorized());

        mvc.perform(get("/api/users/search/me"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("admin")
    public void noPasswordInOutput() throws Exception {
        mvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("@..password").doesNotExist());

        mvc.perform(get("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("@..password").doesNotExist());
    }

    @Test
    @WithUserDetails("admin")
    public void passwordCanBeChanged() throws Exception {
        Optional<User> userBeforeChange = userRepository.findById(3L);

        mvc.perform(patch("/api/users/3").content("{\"password\":\"New password\"}"))
                .andExpect(status().isNoContent());

        Optional<User> userAfterChange = userRepository.findById(3L);

        assertThat(userBeforeChange.isPresent());
        assertThat(userAfterChange.isPresent());

        assertThat(userBeforeChange.get().getPassword()).isNotEqualTo(userAfterChange.get().getPassword());
    }

    @Test
    @WithUserDetails("observer")
    public void observerCanGet() throws Exception {
        assert(AuthenticationUtil.getAuthorities().contains(Role.OBSERVER));

        mvc.perform(get("/api/users"))
                .andExpect(status().isOk());

        mvc.perform(get("/api/users/1"))
                .andExpect(status().isOk());

        mvc.perform(get("/api/users/search/me"))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("admin")
    public void userCanNotBeDeleted() throws Exception {
        assert(AuthenticationUtil.getAuthorities().contains(Role.ADMIN));

        mvc.perform(MockMvcRequestBuilders.delete("/api/users/3"))
                .andExpect(status().isMethodNotAllowed());
    }
}
