package com.igere.issuetracker.data;

import com.igere.issuetracker.model.*;
import com.igere.issuetracker.repository.CommentRepository;
import com.igere.issuetracker.repository.IssueRepository;
import com.igere.issuetracker.repository.ProjectRepository;
import com.igere.issuetracker.repository.UserRepository;
import com.igere.issuetracker.security.AuthenticationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Collections;

@Profile("default")
@Component
public class InsertData {
    public static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod" +
            " tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation" +
            " ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in" +
            " voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non" +
            " proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    IssueRepository issueRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CommentRepository commentRepository;

    @EventListener(classes = ContextRefreshedEvent.class)
    @Transactional
    public void onContextStartedEvent() {
        if (userRepository.count() == 0) {
            User admin = new User("admin", passwordEncoder.encode("admin"), Role.ADMIN, true, Collections.emptySet());
            User user = new User("user", passwordEncoder.encode("user"), Role.USER, true, Collections.emptySet());
            User observer = new User("observer", passwordEncoder.encode("observer"), Role.OBSERVER, true, Collections.emptySet());

            AuthenticationUtil.setAuthentication(admin);

            admin = userRepository.save(admin);

            AuthenticationUtil.setAuthentication(admin);

            user = userRepository.save(user);
            userRepository.save(observer);

            insertDemoProject(admin, user);
            insertIssueTracker(admin, user);

            AuthenticationUtil.clearAuthentication();
        }
    }

    private void insertDemoProject(User admin, User user) {
        AuthenticationUtil.setAuthentication(admin);

        Project project = projectRepository.save(new Project(
                "Demo Project",
                "This project should be considered while exploring application features.\n\n\n" +
                        "For instance this description shows formatting option of project description.\nNow click the title or the edit icon.",
                Collections.emptyList()));

        Issue issue = issueRepository.save(Issue.builder().title("The first issue")
                .description("This is the first issue in issue tracker.\n\nNotice how edit option of comment will be available only for user that is the creator of comment.")
                .priority(1L).project(project).type(Type.NOTE).state(State.OPEN).build());

        commentRepository.save(new Comment("Comment of Admin.", issue));

        AuthenticationUtil.setAuthentication(user);

        commentRepository.save(new Comment("Comment of User.", issue));

        AuthenticationUtil.setAuthentication(admin);

        issue = issueRepository.save(Issue.builder().title("The second issue").description("This issue has large number of comments.").priority(1L).project(project).type(Type.FEATURE).state(State.IN_PROGRESS).build());
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                AuthenticationUtil.setAuthentication(admin);
            } else {
                AuthenticationUtil.setAuthentication(user);
            }

            commentRepository.save(new Comment(String.format("Comment number %1d", i), issue));
        }

        AuthenticationUtil.setAuthentication(admin);

        issueRepository.save(Issue.builder().title("The third issue").description(LOREM_IPSUM).priority(1L).project(project).type(Type.NOTE).state(State.OPEN).build());
        issueRepository.save(Issue.builder().title("The fourth issue").description(LOREM_IPSUM).priority(1L).project(project).type(Type.TASK).state(State.OPEN).build());
        issueRepository.save(Issue.builder().title("The fifth issue").description(LOREM_IPSUM).priority(1L).project(project).type(Type.BUG).state(State.CLOSED).build());
        issueRepository.save(Issue.builder().title("The sixth issue").description(LOREM_IPSUM).priority(1L).project(project).type(Type.BUG).state(State.OPEN).build());
        issueRepository.save(Issue.builder().title("Issue that demonstrates pagination of issues").description(LOREM_IPSUM).priority(1L).project(project).type(Type.FEATURE).state(State.IN_PROGRESS).build());
    }

    private void insertIssueTracker(User admin, User user) {
        AuthenticationUtil.setAuthentication(admin);

        Project issueTracker = projectRepository.save(new Project(
                "Issue Tracker",
                "This is my thesis demonstration project. Aim is to demonstrate how easily REST web services can be created.",
                Collections.emptyList()));

        Issue issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .title("Generic error handling")
                .description("Implement generic error handling. Decide should HAL be used here.").priority(1L).state(State.OPEN).build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Update issue model")
                .description("Add fields type and .").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Project access rights")
                .description("Only admin can CRUD project.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.BUG)
                .state(State.CLOSED)
                .priority(100L)
                .title("Audit")
                .description("Auditing is not working.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Comment access rights")
                .description("All but observer can create comments. Only creator can edit comments.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Audit details")
                .description("Id and audit details should not be changeable.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.TASK)
                .state(State.CLOSED)
                .priority(1L)
                .title("Tests")
                .description("Create test cases for REST API.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Issue sorting")
                .description("Add ability to sort issues by name and priority.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Jackson customization")
                .description("- Indentation\\n- Remove empty\\n- Disable fail on empty beans").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Update issue")
                .description("Add priority to issue.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("CORS")
                .description("Add CORS to REST API so that FE can talk to it.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.TASK)
                .state(State.IN_PROGRESS)
                .priority(1L)
                .title("Test data")
                .description("Add test data. Also add demo data base on actual work track performed during IT creation").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.IN_PROGRESS)
                .priority(1L)
                .title("Create demo FE")
                .description("Create demo FE based on VUE.js.").build());

        issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.IN_PROGRESS)
                .priority(1L)
                .title("README")
                .description("Write README.MD with clarification how to run project.").build());

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("User access rights")
                .description("User can't be deleted.\\nUser can partially edit himself, but can't change its role.").build());

        commentRepository.save(new Comment("Provide URL to access current user.", issue));

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.CLOSED)
                .priority(1L)
                .title("Issue edit rights")
                .description("Only assigned or admin can edit issue.").build());

        commentRepository.save(new Comment("This feature is REJECTED!", issue));

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.BUG)
                .state(State.OPEN)
                .priority(1L)
                .title("Author name of comment")
                .description("Placeholder text is showing instead of author's name.").build());
        commentRepository.save(new Comment("Use excerp of projection to solve this problem.", issue));

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.BUG)
                .state(State.OPEN)
                .priority(1L)
                .title("Description color on issue edit")
                .description("Description color on issue edit does not react on type change.").build());

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.OPEN)
                .priority(1L)
                .title("Creation details for all entities on FE")
                .description("All entities on FE should have creation/audit details displayed.").build());
        commentRepository.save(new Comment("Use excerp of projection to solve this problem for user entity.", issue));
        commentRepository.save(new Comment("Moment.js should be used for time.", issue));

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.BUG)
                .state(State.OPEN)
                .priority(1L)
                .title("Issue state badge text")
                .description("Change 'IN_PROGRESS' to 'In Progress'").build());

        issue = issueRepository.save(Issue.builder()
                .project(issueTracker)
                .type(Type.FEATURE)
                .state(State.OPEN)
                .priority(1L)
                .title("Description/details layout")
                .description("Description/details should be formatted.").build());
        commentRepository.save(new Comment("Use html output with or without preprocessing and consider using preformatted tag.", issue));
    }

}
