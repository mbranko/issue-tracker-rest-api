package com.igere.issuetracker;

import com.igere.issuetracker.model.Comment;
import com.igere.issuetracker.model.Issue;
import com.igere.issuetracker.model.Project;
import com.igere.issuetracker.model.User;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
public class RepositoryRestResourceConfiguration extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setBasePath("/api");
        config.setRepositoryDetectionStrategy(RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED);

        config.exposeIdsFor(User.class, Project.class, Issue.class, Comment.class);
    }
}
