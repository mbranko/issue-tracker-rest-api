package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Issue;
import com.igere.issuetracker.model.Project;
import com.igere.issuetracker.security.IsAdmin;
import com.igere.issuetracker.security.IsUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface IssueRepository extends PagingAndSortingRepository<Issue, Long> {
    Page<Issue> findAllByProject(Project project, Pageable pageable);

    @IsUser
    @Override
    <S extends Issue> S save(S entity);

    @IsUser
    @Override
    <S extends Issue> Iterable<S> saveAll(Iterable<S> entities);

    @IsAdmin
    @Override
    void deleteById(Long aLong);

    @IsAdmin
    @Override
    void delete(Issue entity);

    @IsAdmin
    @Override
    void deleteAll(Iterable<? extends Issue> entities);

    @IsAdmin
    @Override
    void deleteAll();
}
