package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Comment;
import com.igere.issuetracker.model.Issue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {
    Page<Comment> findAllByIssue(Issue issue, Pageable pageable);
}
