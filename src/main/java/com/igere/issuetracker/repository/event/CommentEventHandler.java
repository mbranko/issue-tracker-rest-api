package com.igere.issuetracker.repository.event;

import com.igere.issuetracker.model.Comment;
import com.igere.issuetracker.security.IsCreatorOfComment;
import com.igere.issuetracker.security.IsUser;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler
public class CommentEventHandler {
    @HandleBeforeCreate
    @IsUser
    public void handleBeforeCreate(Comment comment) {
    }

    @HandleBeforeSave
    @IsCreatorOfComment
    public void handleBeforeSave(Comment comment) {
    }

    @HandleBeforeDelete
    @IsCreatorOfComment
    public void handleBeforeDelete(Comment comment) {
    }
}
