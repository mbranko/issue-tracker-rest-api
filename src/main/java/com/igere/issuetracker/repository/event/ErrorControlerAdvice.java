package com.igere.issuetracker.repository.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ErrorControlerAdvice {
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<Error> exceptionHandler(ConstraintViolationException ex) {
        return ex.getConstraintViolations().stream()
                .map(cv -> {
                    return new Error(
                            cv.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName(),
                            cv.getPropertyPath().toString(),
                            cv.getMessage());
                })
                .collect(Collectors.toList());
    }

    @ExceptionHandler(TransactionSystemException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<Error> exceptionHandler(TransactionSystemException ex) throws Throwable {
        Throwable rootCause = ex.getRootCause();
        if (rootCause instanceof ConstraintViolationException) {
            return exceptionHandler((ConstraintViolationException) ex.getRootCause());
        } else
            throw rootCause;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public class Error {
        String errorCode;
        String field;
        String message;
    }
}
