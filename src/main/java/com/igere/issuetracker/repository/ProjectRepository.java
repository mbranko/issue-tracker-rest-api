package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.Project;
import com.igere.issuetracker.security.IsAdmin;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {
    @IsAdmin
    @Override
    <S extends Project> S save(S entity);

    @IsAdmin
    @Override
    <S extends Project> Iterable<S> saveAll(Iterable<S> entities);

    @IsAdmin
    @Override
    void deleteById(Long aLong);

    @IsAdmin
    @Override
    void delete(Project entity);

    @IsAdmin
    @Override
    void deleteAll(Iterable<? extends Project> entities);

    @IsAdmin
    @Override
    void deleteAll();
}
