package com.igere.issuetracker.repository;

import com.igere.issuetracker.model.User;
import com.igere.issuetracker.security.IsAdmin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);

    @IsAdmin
    @Override
    <S extends User> S save(S entity);

    @IsAdmin
    @Override
    <S extends User> Iterable<S> saveAll(Iterable<S> entities);

    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    @RestResource(exported = false)
    @Override
    void delete(User entity);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends User> entities);

    @RestResource(exported = false)
    @Override
    void deleteAll();

    @RestResource(exported = true)
    @Query("select u from User u where u.username = ?#{principal.username}")
    User me();
}
