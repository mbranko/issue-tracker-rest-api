package com.igere.issuetracker.model.projection;

import com.igere.issuetracker.model.User;

import java.time.LocalDateTime;

public interface AuditProjection {
    Long getId();

    Long getVersion();

    LocalDateTime getCreatedAt();

    User getCreatedBy();

    LocalDateTime getModifiedAt();

    User getModifiedBy();
}
