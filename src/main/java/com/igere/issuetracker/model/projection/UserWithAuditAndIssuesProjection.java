package com.igere.issuetracker.model.projection;

import com.igere.issuetracker.model.Issue;
import com.igere.issuetracker.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Projection(name = "auditAndIssues", types = {User.class})
public interface UserWithAuditAndIssuesProjection extends UserWithAuditProjection {
    Set<Issue> getIssues();

    @Value("#{target.issues.size()}")
    Long getIssuesCount();
}
