package com.igere.issuetracker.model.projection;

import com.igere.issuetracker.model.Project;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "audit", types = {Project.class})
public interface ProjectWithAuditProjection extends AuditProjection {
    String getName();

    String getDescription();
}
