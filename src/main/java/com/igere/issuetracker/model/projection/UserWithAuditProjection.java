package com.igere.issuetracker.model.projection;

import com.igere.issuetracker.model.Role;
import com.igere.issuetracker.model.User;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "audit", types = {User.class})
public interface UserWithAuditProjection extends AuditProjection {
    String getUsername();

    Role getRole();

    Boolean getEnabled();
}
