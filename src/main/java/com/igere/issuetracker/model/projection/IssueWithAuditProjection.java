package com.igere.issuetracker.model.projection;

import com.igere.issuetracker.model.Issue;
import com.igere.issuetracker.model.State;
import com.igere.issuetracker.model.Type;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "audit", types = {Issue.class})
public interface IssueWithAuditProjection extends AuditProjection {
    String getTitle();

    String getDescription();

    Long getPriority();

    State getState();

    Type getType();
}
