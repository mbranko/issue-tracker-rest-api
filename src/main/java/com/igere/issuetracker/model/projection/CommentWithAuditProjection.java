package com.igere.issuetracker.model.projection;

import com.igere.issuetracker.model.Comment;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "audit", types = {Comment.class})
public interface CommentWithAuditProjection extends AuditProjection {
    String getContent();
}
