package com.igere.issuetracker.model;

import lombok.Getter;

@Getter
public enum Type {
    FEATURE /* + */,
    TASK /* # */,
    BUG /* ! */,
    NOTE /* * */;
}
