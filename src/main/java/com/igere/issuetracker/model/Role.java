package com.igere.issuetracker.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Role implements GrantedAuthority {
    ADMIN(1L),
    USER(2L),
    OBSERVER(3L);

    private Long id;

    public static Role getRole(String role) {
        return Arrays.stream(Role.values())
                .filter(r -> r.getAuthority().equals(role))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAuthority() {
        return name();
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.stream(Role.values())
                .filter(r -> r.getId() >= this.getId())
                .collect(Collectors.toList());
    }
}
