package com.igere.issuetracker.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "version"})
@Builder
public class Issue extends AbstractBaseEntity {
    @NotBlank
    @Column(unique = true)
    protected String title;

    @Length(max = 1024)
    protected String description;

    @NotNull
    protected Long priority = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @NotNull
    protected Project project;

    @NotNull
    protected State state = State.OPEN;

    @NotNull
    protected Type type;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "assignee",
            joinColumns = @JoinColumn(name = "issue_id"),
            inverseJoinColumns = @JoinColumn(name = "assignee_id")
    )
    protected Set<User> assignees = new HashSet<>();

    @OneToMany(
            mappedBy = "issue",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Comment> comments = new ArrayList<>();

    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setIssue(this);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
        comment.setIssue(null);
    }

    public void addAssignee(User user) {
        assignees.add(user);
        user.getIssues().add(this);
    }

    public void removeAssignee(User user) {
        assignees.remove(user);
        user.getIssues().remove(this);
    }
}
