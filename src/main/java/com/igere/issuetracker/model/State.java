package com.igere.issuetracker.model;

import lombok.Getter;

@Getter
public enum State {
    OPEN,
    IN_PROGRESS,
    CLOSED;
}
