package com.igere.issuetracker.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@PreAuthorize("authentication.principal.username == #comment.createdBy.username")
public @interface IsCreatorOfComment {
}
