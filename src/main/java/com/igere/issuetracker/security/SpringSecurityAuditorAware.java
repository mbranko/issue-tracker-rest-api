package com.igere.issuetracker.security;

import com.igere.issuetracker.model.User;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpringSecurityAuditorAware implements AuditorAware<User> {
    public static final String BEAN_NAME = "springSecurityAuditorAware";

    public Optional<User> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }

        if (authentication.getPrincipal() instanceof UserDetailsAdapter) {
            return Optional.of(((UserDetailsAdapter) authentication.getPrincipal()).getUser());
        }

        return null;
    }
}
