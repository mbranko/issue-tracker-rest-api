# How to curl REST API

Root of API can be accessed with:
```
curl http://admin:admin@localhost:8090/api/
```

## User
```
curl http://admin:admin@localhost:8090/api/users
```

### Create User
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"username":"drx","password":"yes", "role":"USER", "enabled":true}' \
  http://admin:admin@localhost:8090/api/users

curl --header "Content-Type: application/json" --verbose \
  --data '{"username":"cto","password":"mnogodobro", "role":"USER", "enabled":true}' \
  http://admin:admin@localhost:8090/api/users
```

### Update User
```
curl --header "Content-Type: application/json" --verbose \
  -X PUT \
  --data '{"username":"drx","password":"yes", "role":"OBSERVER", "enabled":false}' \
  http://admin:admin@localhost:8090/api/users/6
```
### Partial update of User
```
curl --header "Content-Type: application/json" --verbose \
  -X PATCH \
  --data '{"password":"yes"}' \
  http://admin:admin@localhost:8090/api/users/6
```

### Delete User
By API design users can't be deleted and any try should return 405 (Method Not Allowed).

```
curl -X DELETE --verbose http://admin:admin@localhost:8090/api/users/2
```

## Project

```
curl http://observer:observer@localhost:8090/api/projects
```

### Create Project
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"name":"REST Project","description":"Project that was created via REST."}' \
  http://admin:admin@localhost:8090/api/projects
```

### Delete Project
```
curl -X DELETE --verbose http://admin:admin@localhost:8090/api/projects/6
```
## Issue

```
curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/issues

curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/issues/11

curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/projects/6/issues

curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/projects/6/issues/11
```

### Create Issue
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"title":"REST Project Note","description":"Project that was created via REST.", "state": "OPEN", "type": "NOTE", "project": "/api/projects/16"}' \
  http://admin:admin@localhost:8090/api/issues
```


## Assignee (Issue + User)
### Create Assignee(s)
```
curl --header "Content-Type: text/uri-list " --verbose \
  -X PUT \
  --data 'http://localhost:8090/api/users/1' \
  http://admin:admin@localhost:8090/api/issues/9/assignees

curl --header "Content-Type: text/uri-list " --verbose \
  -X PATCH \
  --data '/api/users/2' \
  http://admin:admin@localhost:8090/api/issues/9/assignees
```
Post will replace assignees list while patch will add to it.

## Comment
```
curl http://observer:observer@localhost:8090/api/comments

curl http://observer:observer@localhost:8090/api/issues/11/comments
```

### Create
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"content":"Somenting is really strange.", "issue": "/api/issues/7"}' \
  http://user:user@localhost:8090/api/comments

curl --header "Content-Type: application/json" --verbose \
  --data '{"content":"Somenting is really strange.", "issue": "/api/issues/7"}' \
  http://admin:admin@localhost:8090/api/comments

```

### Update
```
curl --header "Content-Type: application/json" --verbose \
  --request PUT \
  --data '{"content":"Comment changed by PUT"}' \
  http://admin:admin@localhost:8090/api/comments/8
  
  curl --header "Content-Type: application/json" --verbose \
    --request PATCH \
    --data '{"content":"Comment changed by PATCH"}' \
    http://admin:admin@localhost:8090/api/comments/9
```

### Delete
```
curl --verbose --request DELETE http://admin:admin@localhost:8090/api/comments/11
```
