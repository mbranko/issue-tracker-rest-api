# SPECIFIKACIJA

﻿Kao demonstrativnu aplikaciju mogućnosti Spring Data REST modula treba kreirati applikaciju za issue tracking (praćenje zadataka i problema). Funkcionalnosti aplikacije treba da su prilagođene demonstraciji mogućnosti posmatranog modula Spring-a i ne treba da pokrivaju potpuno domen problema issue trackinga.

## Model podataka

Domenski model sadrži 3 tipa entiteta i to su po hijerarhiji poređani: project (projekat), issue (zadatak) i comment (komentar). Project sadrži kolekciju svojih issue-a, a oni imaju kolekciju svojih comment-a.

Svaki od tipova entiteta sadrži spopstvene informacije vezane za svoju namenu, a zajedničko za sve je da sadrže audit (revizione) podatke. Nije potrebno čuvati istoriju vrednosti već samo podatke o kreaciji i poslednjoj izmeni. Podaci od značaja za izmene su vreme i korisnik koji ih je učinio kao i redni broj verzije entiteta.

Pored entiteta direktno vezanih za domen problema aplikacija sadrži i korisnike. Postoje 3 tipa korisnika, to su admin (administrator), user (korisnik) i observer (posmatrač). Podrazumeva se da administrator ima veća prava od korinika i da može da vrši iste stvari kao korisnik, dok korisnik ima veća prava od posmatrača i da može da vrši iste stvari kao i posmatrač. Dijagram modela dat je na slici 3.1.

![Slika 3.1 - Model podataka](./images/Model.png){ width=100% }

## Slučajevi korišćenja

Samo ulogovani korisnici smeju da pristupaju resursima sistema. Posmatrači imaju prava da vide sve podatke, ali ih ne mogu menjati. Korisnici mogu da kreiraju issue i comment-e, a mogu da vrše izmene nad entitetom tipa issue bez ograničenja dok comment entitet mogu da menjaju samo ako su ga oni i kreirali. Administator kreira korisnike, ali ih ne može brisati. Brisanje korisnika nije moguće. Administrator kreira, ažurira i briše projekte, a može i da briše entitet tipa issue.

Tabelarni prikaz mogućnosti korisnika po entitetu dat je u tabeli 3.1.

| Tip korisnika\Tip entiteta | User | Project | Issue | Comment |
| -------------------------- | ---- | ------- | ----- | ------- |
| Admin | V,A,U | V,A,U,D | V,A,U,D | V,A,U!,D! |
| User | V | V | V,A,U | V,A,U!,D! |
| Observer | V | V | V | V |

> Tabela 3.1

Legenda:

* V - Pravo pregleda (view).
* A - Pravo kreiranja (add).
* U - Pravo izmene (update).
* U! - Uslovno pravo izmene (update).
* D - Pravo brisanja (delete).
* D! - Uslovno pravo brisanja (delete).

## Dodatne napomene

S obzirom da entitet korisnika sadrži password (lozinku) ona se ne sme čuvati u otvorenom obliku. Lozinka ne sme da bude dostupna preko REST API za čitanje, ali je dostupna za unos i izmenu.

