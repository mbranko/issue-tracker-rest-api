# Prilog A

U ovom prilogu su dati primeri korišćenja kreiranog REST API-ja Issue Tracker aplikacije uz pomoć UNIX komande CURL.

Pristup korenu REST API-ja:
```
curl http://admin:admin@localhost:8090/api/
```

## Korisnici
```
curl http://admin:admin@localhost:8090/api/users

curl http://admin:admin@localhost:8090/api/users/search/me
```

### Kreiranje korisnika
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"username":"drx","password":"yes", "role":"USER", "enabled":true}' \
  http://admin:admin@localhost:8090/api/users

curl --header "Content-Type: application/json" --verbose \
  --data '{"username":"cto","password":"beige", "role":"USER", "enabled":true}' \
  http://admin:admin@localhost:8090/api/users
```

### Ažuriranje korisnika
```
curl --header "Content-Type: application/json" --verbose \
  -X PUT \
  --data '{"username":"drx","password":"yes", "role":"OBSERVER", "enabled":false}' \
  http://admin:admin@localhost:8090/api/users/6
```

### Parcijalno ažuriranje korisnika

```
curl --header "Content-Type: application/json" --verbose \
  -X PATCH \
  --data '{"password":"no"}' \
  http://admin:admin@localhost:8090/api/users/6
```

### Brisanje korisnika
Dizajnom API-ja brisanje korisnika nije moguće i svaki pokušaj brisanja će rezultovati odgovorom 405 (Method Not Allowed).

```
curl -X DELETE --verbose http://admin:admin@localhost:8090/api/users/2
```

## Projekat

```
curl http://observer:observer@localhost:8090/api/projects
```

### Kreiranje projekata
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"name":"REST Project","description":"Project that was created via REST."}' \
  http://admin:admin@localhost:8090/api/projects
```

### Brisanje projekata
```
curl -X DELETE --verbose http://admin:admin@localhost:8090/api/projects/6
```
## Zadaci / Issue

```
curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/issues

curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/issues/11

curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/projects/6/issues

curl --header "Content-Type: application/json" --verbose \
  http://admin:admin@localhost:8090/api/projects/6/issues/11
```

### Kreiranje zadataka
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"title":"REST Project Note","description":"Project that was created via REST.", "state": "OPEN", "type": "NOTE", "project": "/api/projects/16"}' \
  http://admin:admin@localhost:8090/api/issues
```

## Dodeljivanje (zadatak + korisnik)
### Kreiranje lista dodeljenih zadataka
```
curl --header "Content-Type: text/uri-list " --verbose \
  -X PUT \
  --data 'http://localhost:8090/api/users/1' \
  http://admin:admin@localhost:8090/api/issues/9/assignees

curl --header "Content-Type: text/uri-list " --verbose \
  -X PATCH \
  --data '/api/users/2' \
  http://admin:admin@localhost:8090/api/issues/9/assignees
```

POST menja celu listu dok PATCH parcijalno azurira listu.

## Komentari
```
curl http://observer:observer@localhost:8090/api/comments

curl http://observer:observer@localhost:8090/api/issues/11/comments
```

### Kreiranje komentara
```
curl --header "Content-Type: application/json" --verbose \
  --data '{"content":"Somenting is really strange.", "issue": "/api/issues/7"}' \
  http://user:user@localhost:8090/api/comments

curl --header "Content-Type: application/json" --verbose \
  --data '{"content":"Somenting is really strange.", "issue": "/api/issues/7"}' \
  http://admin:admin@localhost:8090/api/comments

```

### Ažuriranje komentara
```
curl --header "Content-Type: application/json" --verbose \
  --request PUT \
  --data '{"content":"Comment changed by PUT"}' \
  http://admin:admin@localhost:8090/api/comments/8

  curl --header "Content-Type: application/json" --verbose \
    --request PATCH \
    --data '{"content":"Comment changed by PATCH"}' \
    http://admin:admin@localhost:8090/api/comments/9
```

### Brisanje komentara
```
curl --verbose --request DELETE http://admin:admin@localhost:8090/api/comments/11
```

