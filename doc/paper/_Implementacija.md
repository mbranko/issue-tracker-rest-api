# Implementacija

U ovom poglavlju biće opisana implementacija demonstrativne aplikacije sa fokusom na implementaciju njenog __backend__ dela. Kako __frontend__ tehnologija nije tema ovog rada opis implementacije tog dela aplikacije neće biti dat, ali će u narednom poglavlju biti dat prikaz izgleda aplikacije.

Pregled implementacije će biti izložen prolazom kroz pakete po kojima je kod organizovan. Izgleda paketa dat je na slici 4.1.

![Slika 4.1 - Paketi](./images/packages.png){ width=100% }


## Bazni paket

Bazni paket `com.igere.issuetracker` sadrži klasu sa main metodom za pokretanje aplikacije i konfiguraciju repozitorijuma i bezbednosti. Interesantan i drugačiji koncept organizacije web aplikacije je ovaj koji Spring Boot nudi jer kao produkt dobijamo izvršnu JAR arhivu umesto WAR arhive. Web server dolazi uključen u JAR arhivi pa od tuda i potreba za main metodom.

U listingu 4.1 sledi pregled klase `IssueTrackerApplication` preko koje se pokreće aplikacija.

```
package com.igere.issuetracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IssueTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(IssueTrackerApplication.class, args);
	}
}
```

> Listing 4.1

Bazni paket ujedno čini i __namespace__ koji koriste svi ostali paketi u aplikaciji.

Konfiguracija REST repozitorijuma se nalazi u klasi `RepositoryRestResourceConfiguration` koja je u skraćenom obliku prikazana u listingu 4.2.

```
@Component
public class RepositoryRestResourceConfiguration extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setBasePath("/api");
        config.setRepositoryDetectionStrategy(RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED);

        config.exposeIdsFor(User.class, Project.class, Issue.class, Comment.class);
    }
}
```

> Listing 4.2 

Iz prikazanog koda može se videti kako se konfiguriše bazna putanja, strategija izlaganja rapozitorijuma preko REST interfejsa. Poslednja od korišćenih konfiguracija je promena podrazumevanog ponašanja serijalizacije objekata u JSON sa kojom se u JSON uključuju identifikatori. Identifikatori su uključeni ba bi se olakšao rad sa entitetima na __frontend__-u. Autori Spring Data REST modula su možda želeli da skrivanjem identifikatora resursa nametnu korišćenje linkova što je u skladu sa principima REST-a, ali bi nedostatak identifikatora u resursima otežao rad.

U listingu 4.3 sledi skraćeni prikaz konfiguracije sigurnosti.

```
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EnableJpaAuditing(auditorAwareRef = SpringSecurityAuditorAware.BEAN_NAME)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/h2/**", "/actuator/**")
                .hasAnyAuthority(Role.ADMIN.name())
                .anyRequest().authenticated().and()
                .csrf().disable()
                .headers().frameOptions().sameOrigin().and()
                .cors().and()
                .httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
```

> Listing 4.3

Iz listinga 4.3 konfiguracije sigurnosti može da se zapaze dve putanje kojima je posvećena posebna pažnja u konfiguraciji prava pristupa putanjama to su `/h2/**` i `/actuator/**`.

Prva od pomenutih putanja je putanja na kojoj se nalazi konzola za pristup ugradjenoj bazi podataka. Naime, u projektu je korišćena H2 baza koja dolazi ugradjena u aplikaciji. Ovo je novi koncept koji olakšava i ubrzava razvoj aplikacija, ali i njihovo pokretanje jer nije potrebno dodatno podešavanje baze podataka. U kompleksnijim sistemima sa daleko više podataka bi se sigurno koristila neka druga baza podataka, ali za potrebe razvoja i demonstracije koncepta H2 baza je sasvim zadovoljavajuća.

Druga od pomenutih putanja vezana je za alat Actuator koji predstavlja konzolu za praćenje statusa Spring aplikacije. Actuator nudi pregled mnoštva korisnih parametara pokrenute aplikacije.

U nastavku listinga 4.3 se vidi da je CSRF (Cross-site request forgery) zaštita isključena jer ona nije kompatibilna sa konzolom H2 baze koja je korišćena. Konfiguraciju sigurnosti je moguće podesiti i tako da je konfigurabilno da li je CSRF zaštita uključena ili ne, ali je ovde zbog jednostavnosti zaštita stalno isključena kao što su isključene još neke sigurnosne opcije koje Spring Security pruža.

Kao metod autentifikacije koristi se Http Bacis Authentication koji je izabran zbog jednostavnosti upotrebe i konfiguracije. Autentifikacija preko forme je isključena zbog neželjene nuspojave koju ona donosi. Kada bi autentifikacije preko login forme bila omogućena tada bi za neke neautentifikovane HTTP zahteve odgovor bio 302, redirekt na autentifikacionu formu umesto 401 ili 403 koji su prikladniji kada se radi o REST API aplikaciji.

U specifikaciji je zahtevano da se lozinke korisnika ne čuvaju u otvorenom obliku, iz tog razloga konfiguracija sigurnosti kreira __Bean__ tipa `PasswordEncoder` koji koristi BCrypt algoritam.

Osnovna namena REST API-ja je da bude korišćen od strane drugih aplikacija, za web aplikacije koje se izvršavaju u web pretraživaču potrebno je podesiti CORS (Cross-origin resource sharing) što i čini poslednji deo sigurnosne konfiguracije iz listinga 4.3.

## Paketi model i projection

Paketi model i projection sadrže klase koje čine model podataka i klase koje služe kao projekcije modela, iz tog razloga su i grupisani.

Model je organizovan tako da svi entiteti kojima odgovara tabela u bazi podataka nasledjuju `AbstractBaseEntity` da bi se izbeglo ponavljanje atributa koji su zajednički za sve entitete. Pored entiteta u model paketu se nalaze i enumeracije korišćene kao atributi pojedinih entiteta. U bazi podataka enumerisani atributi se smeštaju kao redni broj enumerisane vrednosti.

U nastavku sledi skraćeni listing 4.4 i listing 4.5 kao objašnjenje izabranih klasa modela.

```
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractBaseEntity {
    @Id
    @GeneratedValue
    protected Long id;

    @Version
    protected Long version;

    @CreatedDate
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected LocalDateTime createdAt;

    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by_user_id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected User createdBy;

    @LastModifiedDate
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected LocalDateTime modifiedAt;

    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by_user_id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected User modifiedBy;
}
```

> Listing 4.4

```
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractBaseEntity {
    @Column(unique = true)
    @NotBlank
    @Length(min = 2, max = 32)
    protected String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank
    protected String password;

    @NotNull
    protected Role role;

    @NotNull
    protected Boolean enabled;

    @ManyToMany(mappedBy = "assignees")
    protected Set<Issue> issues = new HashSet<>();
}
```

> Listing 4.5

Pored standardnih JPA/Hibernate annotacija mogu se primetiti annotacije biblioteke Lombok. Lombok sluzi da generiše set i get metode, konstruktore i mnoge druge metode čije je bi manuelno generisanje bilo dosadno. Takodje se kao nestandardne annotacije mogu zapaziti annotacije biblioteke Jackson. Jackson je biblioteka koja je zaslužna za konverzije izmedju Java objekata i JSON-a. Kao i Lombok Jackson radi generički posao i oslobadja programera od manuelnog generisanja koda za konverziju. Jackson annotacije su korišćene da naznače koje atribute želimo da izuzmemo iz generisanoj JSON-a, ali i na koji način. Moguće je konfigurisati atribut tako da je njegova konverzija moguća samo u jednom smeru i baš taj pristup je iskorišćen za atribut `password`. Uz pomoć `@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)` postignuto je da se lozinka može menjati, ali da nje iz sigurnosnih razloga nema u generisanom JSON-u.

Spring Data REST omogućava da uz pomoć projekcija kontrolišemo generisanje JSON-a, ali na žalost ne podržava istovremeno korišćenje više projekcija u upitu. Ovaj problem je delimično rešen nasledjivanjem projekcija. Osnovna projekcija je `AuditProjection` i nju sve ostale projekcije koje su zadužene za konkretne resurse nasledjuju. Time je postignuto isto što bi se postiglo istovremenim korišćenjem više projekcija. Ovakvo rešenje nije najbolje, ali je jedino moguće. Na slici 4.2 dat je prikaz nasleđivanja projekcija.


![Slika 4.2](./images/projections.png){ width=100% }

## Paketi repository i event

Paketi repository i event sadrže Spring Data repozitorijume i klase koje reaguju na događaje izazvane korišćenjem repozitorijuma.

Kao primer izdvojeni su `UserRepository` i `UserEventHandler` čiji skraćeni listing 4.6 sledi u nastavku.

```
@RepositoryRestResource
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);

    @IsAdmin
    @Override
    <S extends User> S save(S entity);

    @IsAdmin
    @Override
    <S extends User> Iterable<S> saveAll(Iterable<S> entities);

    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    @RestResource(exported = false)
    @Override
    void delete(User entity);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends User> entities);

    @RestResource(exported = false)
    @Override
    void deleteAll();

    @RestResource(exported = true)
    @Query("select u from User u where u.username = ?#{principal.username}")
    User me();
}
```

> Listing 4.6

Iz listinga 4.6 može da se vidi kako je uz pomoć `@RestResource(exported = false)` isključena DELETE metoda za resurs `users` čime je zadovoljen zahtev iz specifikacije da se korisnici ne mogu brisati. Da bi neka HTTP metoda bila isključena potrebno je annotirati sve Java metode koje su vezane za nju, u ovom slučaju to su sve četiri delete metode koje repozitorijum poseduje. U listingu može da se vidi i korišćenje annotacije `@IsAdmin` koja označava da se annotirana metoda može izvršiti samo ako ju je pozvao korisnik sa rolom administratora. Najinteresantnija metoda je metoda kratkog imena `me()` koja vraća resurs koji predstavlja korisnika pozivaoca metode. Nju Spring Data REST smešta na URL `/users/search/me`. U ovome može da se uoči šablon jer Spring Data REST sve dodatne navedene metode pretrage smešta na podputanji `/search/` pod njihovim originalnim nazivom.

```
@Component
@RepositoryEventHandler
public class UserEventHandler {
    @Autowired
    PasswordEncoder passwordEncoder;

    @HandleBeforeCreate
    @HandleBeforeSave
    public void handleBeforeCreate(User user) {
        if (!StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
    }
}
```

> Listing 4.7

`UserEventHandler` prikazan u listingu 4.7 reaguje na kreaciju i izmene entiteta tipa `User` i tom prilikom transformiše lozinku da bi je zaštitio čime zadovoljava specifikacioni zahtev. Postoji bitna stvar koju treba napomenuti vezanu za praćenje događaja nad repozitorijumima, parametar koji obrađivač događaja dobija predstavlja već izmenjen entitet što je i logično. Problem nastaje ako se želi učitati iz baze stanje entiteta pre izmene. Na zalost zbog toga kako je Hibernate funkcioniše učitavanje entiteta iz baze sa starom verzijom nije direktno moguće. Hibernate će pri pokušaju učitavanja entiteta iz baze dostavljati verziju iz __cache__ koja sadrži već izmenjeni entitet. Rešenje za ovaj problem pisanje metoda sa manuelno podešenim __query__-jem koje bi vrednost snimalo u neki DTO (data transfer object). Ovakvo rešenje se može primeniti i na učitavanje pojedinačnih atributa entiteta.

Ostali repozitorijumi i osluškivači događaja su slični pa neće biti navedeni, ali klasa `ErrorControlerAdvice` zaslužuje da joj se prikaže skraćeni listing 4.8 jer predstavlja generičko rešenje za formiranje poruka o validacionim greškama.

```
@RestControllerAdvice
public class ErrorControlerAdvice {
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<Error> exceptionHandler(ConstraintViolationException ex) {
        return ex.getConstraintViolations().stream()
                .map(cv -> {
                    return new Error(
                            cv.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName(),
                            cv.getPropertyPath().toString(),
                            cv.getMessage());
                })
                .collect(Collectors.toList());
    }

    @ExceptionHandler(TransactionSystemException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<Error> exceptionHandler(TransactionSystemException ex) throws Throwable {
        Throwable rootCause = ex.getRootCause();
        if (rootCause instanceof ConstraintViolationException) {
            return exceptionHandler((ConstraintViolationException) ex.getRootCause());
        } else
            throw rootCause;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public class Error {
        String errorCode;
        String field;
        String message;
    }
}
```

> Listing 4.8

## Paket security

Paket security sadrži sigurnosne komponente aplikacije koje se mogu razvrstati u nekoliko grupa.

Klase `UserDetailsAdapter` i `UserDetailsServiceImpl` služe za povezivanje modela aplikacije sa modulom Spring Security. Spring Security [7] je veoma fleksibilan za upotrebu i povezivanje sa bilo kojom Spring aplikacijom se vrši pisanjem implementacija komponenti koje sadrže specifičnosti povezivane aplikacije.

`SpringSecurityAuditorAware` je komponenta koja vrši unos revizija entiteta iz modela podataka. Smeštena je u paket `security` jer koristi sigurnosne funkcionalnosti da bi došla do podataka o korisniku koji vrši izmene. Drugo logišno mesto za ovu klasu bilo bi u paketu `event` jer i praćenje revizija spada u neku vrstu reakcija na događaje.

Klase iz ovog paketa koje treba spomenuti u nazivu počinju sa `Is*` i predstavljaju annotacije koje služe da ograniče pozive metoda i izvršavanje dozvole samo određenim korisnicima koji zadovoljavaju odredjeni uslov. U nastavku će biti dat skraćeni listing 4.9 dva primera.

```
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@PreAuthorize("hasAuthority('USER')")
public @interface IsUser {
}
```

> Listing 4.9

Izlistana annotacija predstavlja klasičan primer kontrole pristuba paziranom na roli (uloz) korisnika. Kompleksnije pravilo pristupa sledi u nastavku u primeru datom u listingu 4.10.

```
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@PreAuthorize("authentication.principal.username == #comment.createdBy.username")
public @interface IsCreatorOfComment {
}
```

> Listing 4.10

U listingu 4.10 može da se vidi da se uz pomoć SpEL pristupa podacima o korisniku koji je vezan za nit koja obrađuje poziv annotirane metode i poredi ih sa vrednošću parametra iz poziva metode. Da bi slika bila jasnija u nastavku sledi i primer annotirane metode uzet iz aplikacionog koda.

```
@HandleBeforeSave
@IsCreatorOfComment
public void handleBeforeSave(Comment comment) {
}
```

> Listing 4.11

Metoda iz listinga 4.11 se nalazi u klasi `CommentEventHandler`. Jedan od načina da se kontroliše pristup resursu je preko kontrole izvršavanja metoda za obradu događaja.

## Paket data

Paket data sadrzi jednu klasu `InsertData` i ona služi da kreira demonstrativne podatke prilikom pokretanja aplikacije kako bi korisnicima dala primer projekta sa pratećim zadacima. Jedino ova klasa koristi Springov mehanizam da na osnovu profila odredi da li će neka komponenta biti uključena u kontekst aplikacije. Kontrola instanciranja komponente postignuta je annotacijom `@Profile("default")`.  Demonstrativni podaci u ovom slušaju služe da poboljšaju korisničko iskustvo, ali mnogo važniju svrhu ovakvi podaci mogu imati kao podaci za podršku testova o čemu će biti reši u nastavku.

## Testiranje

Da bi se osiguralo da implementacija zadovoljava specifikaciju napisani su kontrolni testovi. Testovima su pokriveni svi repozitorijumi, a pored repozitorijuma testirane su i neke druge klase.

Za potrebe testiranja razvijan je i fiksiran poseban set podataka u klasi koja se zove `InsertTestData`. Da bi se osiguralo da se prilikom pokretanja test konteksta u bazu unoste test, a ne demonstrativni podaci korišćeni su Spring profili.

Spring Boot [8] nudi dobru podršku za testiranje jer omogućava podizanje konteksta u test modu i korišćenje instanciranih komponenti, koje se ponašaju kao i kada je pokrenuta aplikacija.

Biblioteke koje su korišćene za pisanje testova su pored Spring-ove test podršle su JUnit i AssertJ. JUnit pruža glavnu podršku za pisanje testova dok AssertJ sluzi za elegantno pisanje i proveru očekivanih rezultata izvršavanja testova. Od velike pomoći je test komponenta `MockMvc` preko koje se vrše zahtevi ka REST repozitorijumima.

