# Zaključak

Spring Data REST predstavlja dobro rešenje za generisanje REST API-ja aplikacija. Posebna prednost mu je to što lako može da koegzistira sa već postojećim ručno napisanim REST API-jem. Nudi mogućnost prilagođavanja potrebama aplikacije, a nudi i klase za ručno generisanje REST interfejsa.

Pri radu je bitno dobro upoznati ponašanje modula jer nije baš sve onako kako bi se očekivalo, o ovome je najviše bilo reči u radu u sekciji implementacije demonstrativnog projekta.

S obzirom da Spring Data REST otvara generički REST API potrebno je značajnije posvetiti pažnju bezbednosnim aspektima jer vrlo lako i neželjeno mogu da se otvore funkcionalnosti i podaci koji ne bi smeli biti dostupni.

Kao što ni REST nije dobro rešenje za sve vrste aplikacije tako ni Spring Data REST nije rešenje za sve. Domen primene bi mu mogle biti aplikacije sa formama gde postoji obrada podataka jednostavnih struktura. Aplikacije koje imaju složene izlazne podatke možda nisu pogodne za implementaciju korišćenjem Spring Data REST modula, ali i kod njih bih savetovao da se deo aplikacije koji je pogodan za REST implementira korišćenjem pomenutog modula.

