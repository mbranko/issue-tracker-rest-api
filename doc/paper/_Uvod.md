# Uvod

Tema ovog rada je modul Spring Data REST, jedan od modula Spring __framework__-a (radni okvir). Pomenuti modul će biti iskorišćen prilikom implementacije demonstrativnog projekta. Iskustva stečena prilikom implementacije će biti navedena u okviru ovog rada.

Funkcionalnosti koje su implementirane u demonstrativnoj aplikaciji korišćenjem Spring __framework__-a su mogle biti implementirane i na druge načine bez korišćenja Spring Data REST modula, ali bi to zahtevalo daleko složeniji kod. Ovaj rad služi i kao primer kako posmatrani modul ubrzava razvoj aplikacija. Pored Spring Data REST koji predstavlja glavnu temu rada korišćen je i modul Spring Data JPA. Cilj je da se istraži na koji način se koristi Spring Data REST kroz implementaciju aplikacije koja predstavlja issue tracker (sistem za evidenciju zadataka). Svrha primer aplikacije je implementacija funkcionalnosti korišćenjem Spring Data REST modula radi njegove analize i demonstracije, dok sam Spring __famework__ kao takav nije u fokusu.

Kako bi demonstrativna aplikacija bila kompletna razvijen je i deo aplikacije sa korisničkim interfejsom preko koga je moguća interakcija i korišćenje razvijanih funkcionalnosti. Za izradu ovog dela aplikacije razvijena je zasebna podaplikacija korišćenjem modernih web tehnologija. Graficki interfejs je razvijen korišćenjem biblioteke Vue.js, ali ona u okviru ovog rada ostaje van fokusa.

U narednim poglavljima biće više reči o samoj tehnologiji razmatranoj u ovom radu, specifikaciji primer aplikacije uz pružanje uvida u način implementacije i osvrt na probleme koji su se pojavili prilikom razvoja.

Naglasio bih da aplikacija primer ima namenu prikaza izrade REST API-ja izabranom tehnologijom, a da joj nije cilj rešavanja svih problema iz domena issue tracking-a. Ovakav pristup izabran je da bi se više pažnje posvetio opisu tehnologije. Ipak, zbog širokog spektra mogućnosti Spring Data REST modula u ovom radu će biti prikazane one koje su najznačajnije za domen primer aplikacije. Takođe će biti opisana rešenja nekih problema koji su uočeni prilikom njene izrade.

