# Tehnologije

U ovom poglavlju će biti opisane glavne tehnologije za izradu web baziranih sistema koje su i upotrebljene prilikom izradi demonstrativnog projekta. U fokusu će biti tehnologije koje su korišćene na backend delu aplikacije. U nastavku sledi pojašnjenje šta je to backend i kako su zapravo moderne web aplikacije organizovane.

Radi pojašnjenja termina backend i termina frontend koji uz njega ide grubo bih opisao strukturu web aplikacija kao i njihovo funkcionisanje. Web aplikacije kao i većina aplikacija imaju svoj korisnički interfejs, biznis logiku za transformaciju podataka unetih kroz korisnički interfejs i mogućnost smeštanja podataka dobijenih radom biznis logike.

Ranije je zbog ograničenih mogućnosti web pretraživača praksa bila da se korisnički interfejs generiše na serveru i da se tako generisan isporučuje web pretraživaču. Interakcija izmedju korisnika i aplikacije bi bila ostvarivana tako što bi se prilikom svake akcije slao zahtev aplikaciji (serveru) koji bi rezultovao učitavanjem nove stranice rezultata. Za generisanje stranice u Java svetu najviše je korišćen JSP (JavaServer Pages), ali su postojali i drugi __template__ jezici i sistemi. Templejti kao deo korisničkog interfejsa su bili sastavni deo izvornog koda aplikacije i obrađivala ih je ista aplikacija koja je sadržala i ostatak aplikacije sa biznis logikom i sistemom za skladištenje podataka.

Ovakvo korisničko iskustvo pri korišćenju web aplikacija bilo je lošije od iskustva koje bi korisnik imao pri korišćenju desktop aplikacija. Time je nastala potreba da se korisničko iskustvo učini takvim da rad na web aplikacijama izgleda tečnije, bez nepotrebnog ponovnog učitavanja stranica. Vremenom je u razvoj web aplikacija bio uključen JavaScript i njegove biblioteke i neretko je na stranicama u JavaScriptu bila duplirana biznis ili interfejs logika koja je bila implementirana u templejtima koji su obradjivani na serveru.

Razvojem web pretrazivača uz razvoj JavaScript jezika kao i pojava mobilnih aplikacija došlo je do mogućnosti, ali i potrebe da se ista biznis logika otvori ka različitim korisničkim interfejsima koji bi imali iste ili slične funkcionalnsti. Takođe korisnički interfejs web aplikacija je često dolazio u dve varijante, verijanti za desktop web pretraživač i u skromnijoj verziji prilagodjenoj za manje ekrane web pretraživača mobilnih uredjaja. Ovaj trend je stvorio potrebu za raslojavanje i razdvajanje aplikacije na frontend koji predstavlja korisnički interfejs i backend koji prestavlja aplikativnu logiku. Backend poseduje sopstveni interfejs koji predstavlja API (application programming interface) za korisnički interfejs. API backend-a se takodje može koristiti i za komunikaciju sa backend-ima drugih aplikacija. Ovde bih želeo da napomenem da su mrežno bazirani sistemi sastavljeni od više aplikacija i ranije postojali, ali u nekom drugom obliku. Tehnologije kojima su stari mrežno bazirani sistemi razvijani su bile teške za korišćenje i često su zahtevale veliku količinu koda čak i za najjednostavnije funkcionalnosti. Ovaj rad bi trebalo da pokaže koliko je razvoj jednog tipa web aplikacija lakši korišćenjem Spring Data REST modula.

## REST

REST (Representational State Transfer) prestavlja arhitektonski stil mrežno baziranih aplikacija, definisao ga je Roy Thomas Fielding u svojoj doktorskoj disertaciji [1]. Predstavlja set pravila koja se primenjuju za izgradnju web baziranih aplikacija. Nastao je na osnovu izbora pogodnih arhitektonskih stilova uzimajući u obzir njihove pojedinačne prednosti i mane, a sa ciljem kreiranja stila koji je optimalan u opštem slučaju.

Od postavljenih pravila REST arhitekturom naglasio bih da je to klijent-server arhitektura kod koje je postavljen zahtev za unificiranim interfejsom. Unificirani interfejs olakšava izgradnju složenog sistema olakšavajući interakcije izeđu njegovih komponenti. Kao negativnu osobinu unificirani interfejs smanjuje efikasnost sistema jer podaci nisu prilagodjeni konkretnom slučaju korišćenja.

Svaku informaciju REST smatra resursom. Resursi ne moraju direktno predstavljati entitete u sistemu već njihovo značenje. Moguće je da isti entitet bude predstavljen kao dva različita resursa ili da više entiteta bude spregnuto u jedan resurs. Interakcija izmedju komponenti sistema odvija se razmenom resursa koji u toj interakciji mogu biti predstavljeni svojim identifikatorom. U ovom slučaju identifikatorom se smatra URL (uniform resource locator), a ne primarni ključ entitata koji je predstavljen resursom.

REST je __stateless__ što znači da svaki zahtev ka REST sistemu treba da sadrži sve potrebne informacije za njegovu obradu i da se ne može računati na neko sačuvano stanje na serveru tzv. sesiju.

HATEOAS (Hypermedia as the Engine of Application State) je princip REST-a po kome je potrebno obezbediti način za otkrivanje i navigaciju interfejsom. Jednostavno objašnjenje se može naći na [2].

## Spring i Spring Boot

Spring framework je open source (otvorenog koda) __framework__ nastao 2002. godine i veoma je popularan u Java svetu. Namena mu je kreiranje kompleksnih aplikacija gde je jaka konkurencija EJB-u (Enterprise JavaBeans) [3]. Tokom godina evoluirao je uporedo sa programskim jezikom Java i trenutno se nalazi u verziji 5 koja kao minimalnu verziju Jave zahteva verziju 8. Sastavljen je od mnoštva modula koje pokrivaju celokupni spektar potreba kompleksnih aplikacija.

Osnovni koncepti zastupljeni u Spring-u (Spring framework) su __dependency injection__ / __inversion of control__ (inverzija kontrole) i __convention over configuration__ (konvencija pre konfiguracije). Inverzija kontrole je postignuta time što Spring aplikacija predstavlja kontejner za komponente koje mogu biti zavisne jedne od drugih. Svaka komponenta ume da kreira sebe, a komponente od kojih zavisi će joj biti pružene automatski od strane Spring kontejnera. Komponente se u terminologiji Springa nazivaju __bean__-ovi i one mogu biti različitog tipa. One dolaze konfigurisane po konvenciji, a njihovom korisniku je ostavljena mogućnost da konfiguraciju izmeni ako je potrebno. Na navedenim principima zasnovani su moduli od kojih se __framework__ sastoji.

Ranije navedene glavne koncepte Spring __framework__-a pogotovu zapažamo pri radu sa Spring Boot modulom koji je korišćen kao osnova za razvoj primer aplikacije u okviru ovog rada. Spring Boot je osmišljen tako da ubrza razvoj aplikacija nudeći __default__ (uobičajnu) konfiguraciju modula koji se uključuju u sastav razvijane aplikacije. Takodje Spring Boot vodi računa o verzijama modula tako da je početak razvoja dodatno olakšan. Korišćenjem Spring Boot modula vrlo brzo se moze preći na implementaciju biznis logike jer je dodatna konfiguracija od one sa kojom Spring Boot već dolazi često minimalna. Ovo je veoma olakšalo razvoj i dodatno podiglo popularnost Spring __framework__-a. Spring __framework__ je veoma fleksibilan tako da kada je potrebno uobičajna konfiguracija se lako može izmeniti i prilagoditi potrebama aplikacije.

## Spring Data JPA

Spring Data predstavlja skup modula Spring __framework__-a zaduženih za rad sa skladištima podataka. Nudi veći broj podmodula orjentisanih ka konkretnom tipu skladišta podataka. Korišćeni podmodul Spring Data JPA [4] služi za rad sa relacionim bazama podataka. Pruža generički sloj za manipulaciju podacima i time znatno olakšava razvoj aplikacija.

Osnovna abstrakcija koju Spring Data JPA koristi je __repository__ (repozitorijum). U okviru ovog modula postoje vise vrsta __repository__ interfejsa koji nude različite funkcionalnosti za pristup podacima. Kreiranje konkretnog repozitorijuma vrši se nasleđivanjem interfejsa koji sadrži željene funkcionalnosti. Interfejsi koji se nasleđuju su __template__ (šablonski) sa prvim parametrom koji predstavlja tip entiteta koji repozitorijum obrađuje i drugim parametrom koji predstavlja tip primarnog ključa obrađivanog entiteta. Repozitorijumi ponuđeni za nasleđivanje nude mnoštvo već postojećih metoda za rad sa entitetima kao što su standardne CRUD metode (create, update, delete - kreiranje, ažuriranje i brisanje) kao i razne metode za osnovne pretrage kao što su metode za pronalaženje svih entiteta i metode za pronalaženje entiteta na osnovu primarnog ključa. Nije potrebno pisati implementaciju ovih metoda već ce ona biti automatski pružena.

Pored nasleđenih metoda moguće je deklarisati i nove metode čije ime treba da prati specifikaciju koja je propisana u dokumentaciji i modul će i za njih automatski pružiti implementaciju. Ovo predstavlja veoma moćan koncept koji znatno ubrzava razvoj sloja za manipulaciju podacima. Korisnicima modula je dodatno na raspolaganje stavljen i mehanizam gde oni sami mogu da odrede šta će definisane metode raditi tako da je pružena mogućnost i za izvršavanje veoma specifičnih funkcionalnosti.

Funkcionalnosti koje modul dodatno i automatski podržava su sortiranje i paginacija, a da bi ih modul za nas implementirao sve što treba da se uradi je da se navedu kao dodatni parametri deklarisanih funkcija. Treba naglasiti da parametar za paginaciju u sebi već sadrži podatke za sortiranje pa kada se koristi nije potrebno dodatno navoditi parametar za sortiranje.

## Spring Data REST

Spring Data REST [5] je modul za ekstenziju sloja za manipulaciju podacima pružajući mogućnost da se definisani repozitorijumi otvore i izlože kao REST API. On zapravo predstavlja sam po sebi Spring aplikaciju i kao takav može da stoji uz već postojeću aplikaciju ili može da predstavlja osnovu pored koje bi se izgradila nova aplikacija.

### Kontrola izlaganja repozitorijuma

Radi kontrole koji od postojećih repozitorijuma ce biti uključeni u REST API omogućen je izbor jedne od nekoliko strategija čiji pregled je dat u tabeli 2.1


| Naziv strategije | Opis strategije |
| ---------------- | --------------- |
| DEFAULT | Podrazmevana opcija koja izlaže sve javno dostupne repozitorijume pri tome konsultujuci exported marker. |
| ALL | Svi repozitorijumi će biti izloženi bez obzira na vidljivost ili marker exported. |
| ANNOTATION | Izlažu se samo repozitorijumi koji su anotirani sa @(Repository)RestResource , a kojima marker exported nije setovan na false. |
| VISIBILITY | Samo javni anotirani repozitorijumi će biti izloženi. |

> Tabela 2.1

### Postavljanje baznog URL-a

Ako je potrebno moguće je konfigurisati bazu URL putanje u okviru web aplikacije na kojoj će biti dostupni REST repozitorijumi. Ova opcija je korisna da bi se izbegla preklapanja sa već postojećim ili planiranim putanjama.

Bazna putanja se konfiguriše preko `spring.data.rest.basePath` podešavanja koje se najčešće podešava u fajlu `application.properties`, ali može biti podešeno i u Java konfiguraciji aplikacije.

### Pregled mogućih podešavanja

U tabeli 2.2 je dat pregled mogućih podešavanja.


| Naziv i ključ podešavanja | Opis podešavanja |
| ------------------------- | ---------------- |
| basePath | Osnova putanje na kojoj se nalaze REST repozitorijumi. |
| defaultPageSize | Podrazmevani broj entiteta po stranici pri upotrebi paginacije. |
| maxPageSize | Maksimalni broj entiteta po stranici pri upotrebi paginacije. |
| pageParamName | Naziv parametra broja stranice. |
| limitParamName | Naziv parametra limita broja elemenata u stranici. |
| sortParamName | Naziv parametra koji se upotrebljava za definisanje sortiranja. |
| defaultMediaType | Podrazumevana vrednost media type koja će se koristiti ako druga nije specificirana. |
| returnBodyOnCreate | Kontroliše da li će se prilikom kreiranja resursa vraćati vrednost u telu odgovoru servera. |
| returnBodyOnUpdate | Kontroliše da li će se prilikom ažuriranja resursa vraćati vrednost u telu odgovora servera. |

> Tabela 2.2

### Resursti, imenovanje i njihovo otkrivanje

U poglavlju koje ukratko opisuje REST tehnologiju navedeno je da svaka informacija predstavlja resurs. Spring Data REST takodje poseduje pojam resursa i njima predstavlja kolekcije entiteta, pojedinačni entiteti, atributie entiteta kao i asocijacije između entiteta. Pod entitetom se podrazumeva entitet koga modeluje tip koji je prosledjen kao parametar prilikom definisanja repozitorijuma. Primer definisanja repozitorijuma je dat u listingu 2.1.

```
@RepositoryRestResource
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
        User findByUsername(String username);
}
```

> Listing 2.1

Ime resursa definisanog repozitorijumom se automatski dobija na osnovu imena klase entiteta u malim slovima prevodjenjem u množinu pravilima engleskog jezika. U slučaju repozitorijuma iz primera ime resursa će biti “users”. Ostavljena je mogućnost da se ime resursa specificira i ručno preko atributa annotacije `@RepositoryRestResource` pod nazivom “path”.

Kao dodatnu mogućnost, a da bi zadovoljio HATEOAS princip Spring Data REST nudi mogućnost otkrivanja i pregleda dostupnih resursa. Za ovu svrhu je iskorišćen HAL (Hypertext Application Language) format [6]. Na primeru će biti predstavljeno kako otkrivanje resursa izgleda u praksi. REST API na kome su primeri zasnovani je API koji pruža demonstrativna aplikacija, a HTTP zahtevi su rađeni uz pomoć komande CURL.

Ako se uputi zahtev na koren REST API-a uz pomoć komande u listingu 2.2 (koja sadži uputstvo za prikaz detalja):

```
curl -v http://admin:admin@localhost:8090/api/
```

> Listing 2.2

Odgovor je prikazan u listingu 2.3.

```
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8090 (#0)
* Server auth using Basic with user 'admin'
> GET /api/ HTTP/1.1
> Host: localhost:8090
> Authorization: Basic YWRtaW46YWRtaW4=
> User-Agent: curl/7.58.0
> Accept: */*
>
< HTTP/1.1 200
< Set-Cookie: JSESSIONID=073AFB94EB89AC9442B234586DAD38CA; Path=/; HttpOnly
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: SAMEORIGIN
< Content-Type: application/hal+json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Thu, 27 Sep 2018 09:07:57 GMT
<
{
  "_links" : {
        "issues" : {
          "href" : "http://localhost:8090/api/issues{?page,size,sort,projection}",
          "templated" : true
        },
        "comments" : {
          "href" : "http://localhost:8090/api/comments{?page,size,sort,projection}",
          "templated" : true
        },
        "projects" : {
          "href" : "http://localhost:8090/api/projects{?page,size,sort,projection}",
          "templated" : true
        },
        "users" : {
          "href" : "http://localhost:8090/api/users{?page,size,sort,projection}",
          "templated" : true
        },
        "profile" : {
          "href" : "http://localhost:8090/api/profile"
        }
  }
* Connection #0 to host localhost left intact
}
```

> Listing 2.3

Iz dobijenog odgovora može da se uoči da telo odgovora sadrži JSON (JavaScript Object Notation), ali je on proširen dodatnih podtipom HAL što je i specificirano u zaglavlju `Content-Type` sa vrednošću `application/hal+json`. Odgovor zapravo predstavlja skup linkova ka osnovnim entitetima u sistemu. Linkovi su šablonski i nude mogućnost prosleđivanja parametara čija imena su navedena izmedju vitičastih zagrada na kraju linka. Za resurs users link izgleda kao u listingu 2.4.

```
http://localhost:8090/api/users{?page,size,sort,projection}
```

> Listing 2.4

Link ka resursu users možemo ispratiti koristeći i neki od ponuđenih parametara. Radi ograničavanja veličine odgovora koristiće se parametar upita `size` kojim specificiramo željenu veličinu stranice kao što je dato u listingu 2.5.

```
curl "http://admin:admin@localhost:8090/api/users?size=1"
```

> Listing 2.5

Na ovaj upit se dobija odgovor dat u listingu 2.6.


```
  "_embedded" : {
        "users" : [ {
          "id" : 1,
          "createdAt" : "2018-09-27T11:01:42.963",
          "modifiedAt" : "2018-09-27T11:01:42.963",
          "username" : "admin",
          "role" : "ADMIN",
          "enabled" : true,
          "_links" : {
            "self" : {
              "href" : "http://localhost:8090/api/users/1"
            },
            "user" : {
              "href" : "http://localhost:8090/api/users/1{?projection}",
              "templated" : true
            },
            "createdBy" : {
              "href" : "http://localhost:8090/api/users/1/createdBy{?projection}",
              "templated" : true
            },
            "issues" : {
              "href" : "http://localhost:8090/api/users/1/issues{?projection}",
              "templated" : true
            },
            "modifiedBy" : {
              "href" : "http://localhost:8090/api/users/1/modifiedBy{?projection}",
              "templated" : true
            }
          }
        } ]
  },
  "_links" : {
        "first" : {
          "href" : "http://localhost:8090/api/users?page=0&size=1"
        },
        "self" : {
          "href" : "http://localhost:8090/api/users{&sort,projection}",
          "templated" : true
        },
        "next" : {
          "href" : "http://localhost:8090/api/users?page=1&size=1"
        },
        "last" : {
          "href" : "http://localhost:8090/api/users?page=2&size=1"
        },
        "profile" : {
          "href" : "http://localhost:8090/api/profile/users"
        },
        "search" : {
          "href" : "http://localhost:8090/api/users/search"
        }
  },
  "page" : {
        "size" : 1,
        "totalElements" : 3,
        "totalPages" : 3,
        "number" : 0
  }
}
```

> Listing 2.6

Za razliku od predhodnog odgovora ovaj odgvoro je znatno veći, a razlog tome je što predstavlja odgovor na upit kolekcije resursa. U odgovoru je moguće zapaziti nekoliko sekcija kojima su grupisani podaci srodnih namena. To su sekcije “page” koja sadrži podatke o paginaciji, sekcija “_links” i sekcija “_embedded”.

U sekciji “page” se nalaze informacije o paginaciji.

| Atribut | Značenje |
| ------- | -------- |
| size | Veličina stranice. |
| totalElements | Ukupan broj elemenata. |
| totalPages | Ukupan broj stranica. |
| number | Broj trenutne stranice. |

> Tabela 2.3

Sekcija “_links” sadrži mnoštvo linkova čije objašnjenje se nalazi u tabeli 2.4.

| Naziv linka | Vrednost | Značenje |
| ----------- | -------- | -------- |
| first | http://localhost:8090/api/users?page=0&size=1 | Link ka prvoj stranici rezultata. |
| self | http://localhost:8090/api/users{&sort,projection} | Link ka samom sebi. Treba zapaziti da iz šablona nedostaju parametri koji su upotrebljeni. |
| next | http://localhost:8090/api/users?page=1&size=1 | Link ka sledećoj stranici rezultata. S obzirom da upit sadrži prvu stranicu rezultata među linkovima se ne nalazi link “prev” koji bi vodio na prethodnu stranicu rezultata. |
| last | http://localhost:8090/api/users?page=2&size=1 | Link ka poslednjoj stranici rezultata. |
| profile | http://localhost:8090/api/profile/users | Link ka profilu resursa radjenom po Application-Level Profile Semantics (ALPS) na kome se nalazi jednostavan opis značenja entiteta. |
| search | http://localhost:8090/api/users/search |	Link koji sadrzi funkcije za pretragu kolekcije resursa. O ovome će više reči biti u nastavku. |

> Tabela 2.4

Sekcija “_embedded” sadrži vrednoti kolekcije resursa, ona postoji samo za resurse tipa kolekcije dok je za pojedinačne resurse vrednost direktno ugradjena u odgovor bez obuhvatajućeg elementa. Svaki element iz kolekcije resursa ima i svoju “_links” sekciju.

Pristup pojedinačnom entitetu je moguć praćenjem linka iz odgovora ka kolekciji resursa. Za resurs iz kolekcije “users” to možemo ostvariti sa upitom iz listinga 2.7.

```
curl http://admin:admin@localhost:8090/api/users/1
```

> Listing 2.7

Odgovor u ovom slučaju izgleda kao u listingu 2.8.

```
{
  "id" : 1,
  "createdAt" : "2018-09-27T11:01:42.963",
  "modifiedAt" : "2018-09-27T11:01:42.963",
  "username" : "admin",
  "role" : "ADMIN",
  "enabled" : true,
  "_links" : {
        "self" : {
          "href" : "http://localhost:8090/api/users/1"
        },
        "user" : {
          "href" : "http://localhost:8090/api/users/1{?projection}",
          "templated" : true
        },
        "createdBy" : {
          "href" : "http://localhost:8090/api/users/1/createdBy{?projection}",
          "templated" : true
        },
        "issues" : {
          "href" : "http://localhost:8090/api/users/1/issues{?projection}",
          "templated" : true
        },
        "modifiedBy" : {
          "href" : "http://localhost:8090/api/users/1/modifiedBy{?projection}",
          "templated" : true
        }
  }
}
```

> Listing 2.8

Pored osnovnih informacija ukljušenih u koren JSON-a i ovde je prisutna sekcija “_links”, ali je moguće zapaziti nove linkove. Novi linkovi “createdBy” i “issues” predstavljaju asocijacije resursa sa drugim resursima. Asocijacije su predstavljene linkovima kada za njih postoje izloženi repozitorijumi u suprotnom će biti ugrađene u telo odgovora.

### Uticanje na sadržaj tela odgovora

Ponekad je zgodno uticati na sadržaj tela odgovora. Iz prethodnih primera smo videli da odgovor nekada može biti veoma velik, a da nam zapravo nisu korisne sve informacije koje on sadrži. Dalje neke od informacija mogu biti i poverljive prirode pa je njihovo otkrivanje neželjeno. Moguće je uticati na sadržaj tela odgovora na nekoliko načina. Svaki od načina je potrebno specificirati prilikom kreiranja REST API-ja da bi ih korisnik mogao upotrebiti. Prvi od načina korišćenjem paginacije je već spomenut. Pored njega moguće je kreirati projection (projekcija), excerpt (izvod), ali i direktno u entitetima naznačiti ako neki atribut ne želimo da otkrivamo.

Projection i excerpt su u suštini ista stvar jer excerpt predstavlja projection primenjen na resursu tipa kolekcije i uvek je aktivan dok se upotreba projekcije mora dodatno specificirati. Projection je moguce koristiti na kolekciji ili pojedinačnom resursu.

Implementiraju se kreiranjem interfejsa sa metodama čiji nazivi odgovaraju nazivima get-er metoda entiteta modelovanih resursima. Prilikom upotrebe projection-a resurs će sadržati samo one atribute koje interfejs projection-a poseduje. Na žalost nije moguće navesti više od jednom projection-a prilikom postavljanja upita. Načini za otklanjanje ovog nedostatka biće objašnjeni u poglavlju koje se bavi implementacijom demonstrativnog projekta.

U okviru interfejsa projection-a moguće je definisati i get metode koje nemaju ekvivalenta u entitetu. Ovo se koristi za pružanje informacija koje entitet direktno ne poseduje. Ovakvu get metodu je potrebno annotirati sa annotacijom @Value kojoj kao vrednost treba dodeliti SpEL (Spring Expression Language) izraz.

### Operacije nad resursima

Do sada su spomenute samo operacije pristupa resursima, ali pored njih resurse je potrebno kreirati, menjati i brisati. Izmedju određenih resursa mogu postojati i asocijacije pa je manipulacija nad njima takođe potrebna. Sve ovo navedeno je moguće uz pomoć Spring Data REST modula, u nastavku sledi detaljnije objašnjeno kako se navedene operacije izvršavaju.

Iako nije bilo do sada eksplicitno naznačeno jer nije ni bilo potrebe sve u okviru ovog rada što se odnosi na REST i Spring Data REST modul odnosi se na njihovu upotrebu preko HTTP-a (Hypertext Transfer Protocol). Metode protokola imaju jasno definisanu upotrebu u okviru REST-a. U nastavku u tabeli 2.5 sledi pregled metoda HTTP-a uz njihovu ulogu u ovkiru REST-a.

| HTTP Metoda | Upotreba u okviru Spring Data REST-a |
| ----------- | ------------------------------------ |
| GET | Dobavljanje resursa. Primenjuje se na kolekcijama i pojedinačnim resursima. |
| POST | Kreiranje resursa. Primenjuje se na resursima kolekcijama prilikom kreiranja novog resursa kolekcije. |
| PUT | Izmena resursa. Primenjuje se na pojedinačnim resursima. |
| PATCH | Parcijalna izmena resursa. Primenjuje se nad pojedinačnim resursima. |
| DELETE | Brisanje resursa. Primenjuje se nad pojedinačnim resursima. |

> Tabela 2.5

### Pretraga resursa

Spring Data JPA kao što je već navedeno daje mogućnost lakog kreiranja metoda za pretragu entiteta koje repozitorijum obradjuje. Ove metode mogu biti dostupne i preko REST-a ako su tako konfigurisane. Dostupne metode se nalaze u okviru “search” linka sekcije “_links”.

Za primer resursa users na upit iz listinga 2.9.

```
curl http://admin:admin@localhost:8090/api/users/search
```

> Listing 2.9

Dobio bi se odgovor dat u listingu 2.10.

```
{
  "_links" : {
        "findByUsername" : {
          "href" : "http://localhost:8090/api/users/search/findByUsername{?username,projection}",
          "templated" : true
        },
        "me" : {
          "href" : "http://localhost:8090/api/users/search/me{?projection}",
          "templated" : true
        },
        "self" : {
          "href" : "http://localhost:8090/api/users/search"
        }
  }
}
```

> Listing 2.10

U odgovoru su pored “self” linka i linkovi kad dvema metodama pretrage, to su “findByUsername” i “me”. Linkovi su šablonski i njihovi parametri su dati pod vitičastim zagradama.

### Definisanje reakcija na akcije nad repozitorijumom

Moguće je u skladu sa potrebama definisati reakcije na akcije izvršene nad REST repozitorijumom. Postoji više načina da se to uradi, ali svakako najjednostavniji način je uz pomoć definisanja Spring komponente annotirane sa `@RepositoryEventHandler` koja bi sadržala javnu metodu annotiranu sa odgovarajućom annotacijom u zavisnosti od događaja koji želimo da pratimo.

Događaji nad repozitorijumom koje možemo pratiti su:

- BeforeCreateEvent
- AfterCreateEvent
- BeforeSaveEvent
- AfterSaveEvent
- BeforeLinkSaveEvent
- AfterLinkSaveEvent
- BeforeDeleteEvent
- AfterDeleteEvent

Pored toga korisno je znati da je moguće iskoristiti i Spring-ov mehanizam za definisanje globalnog praćenja izuzetaka. Ovo se postiže kreiranjem komponente annotirane sa `@RestControllerAdvice` koja u sebi treba da ima odgovarajuću javnu metodu annotiranu sa `@ExceptionHandler` kojoj je kao vrednost prosleđena klasa izuzetka kojeg želimo da uhvatimo.

Hvatanje izuzetaka može biti iskorišćeno i za formiranje unificiranih validacionih grešaka. O validaciji će biti reči u nastavku.

### Validacija

Prilikom kreiranja i izmene resursa potrebno je izvršiti proveru validnosti njihovih vrednosti. Spring Data REST daje mogućnost registracije validatora u vidu validacionih komponenti. Komponente se mogu registrovati ručno, ali moguća je i automatska registracija prilikom koje komponente treba da prate konvenciju imenovanja kako bi sistem umeo da ih iskoristi. Konvencija imenovanja se sastoji u tome da ime komponente treba da počinje sa “beforeCreate” ili “beforeSave” kako bi kompoenta reagovala na željeni događaj.

Ovo nije jedini način na koji se validacija može izvršiti, bar u slučaju kada Spring Data REST koristi Spring Data JPA moguće je entitete i njihove atribute annotirati sa ograničenjima vrednosti i time postići validaciju. Da bi ovaj pristup bio potreban neophodno je hvatati i obrađivati izuzetke `ConstraintViolationException` i `TransactionSystemException`.

### Sigurnost i autorizacija pristupa repozitorijumima

Spring Security modul zadužen za sigurnost vrlo lako može biti iskorišćen za kontrolu pristupa REST repozitorijumima kontrolišući pristup do najsitnijih detalja. Moguće je kontrolisati pristup na osnovu URL-a, metode pa čak i parametara akcija koje se vrše nad repozitorijumom. Kontrola se postiže na više načina. Globalna pravila pristupa se postavljaju preko konfiguracije HttpSecurity dok se na nivou pojedinačnog repozitorijuma i njegovih metoda mogu koristiti annotacije za konfiguraciju prava pristupa. Annotacije za konfiguraciju prava pristupa su `@Secured`, `@PreAuthorize` i `@PostAuthorize`. Prva annotacija `@Secured` je bazirana na roli korisnika dok annotacije `@PreAuthorize` i `@PostAuthorize` imaju znanto veće mogućnosti kontrole i sa njima je moguće ostvariti kontrolu i na nivou parametara poziva funkcija koristeći SpEL. Da bi kontrola pristupa sa annotacijama funkcionisala potrebno ju je aktivirati sa `@EnableGlobalMethodSecurity` konfiguracionom annotacijom.

S obzirom da su REST repozitorijumi namenjeni za korišćenje od strane drugih aplikacija u slučaju kada su one web aplikacije koje izvršava web pretraživač potrebno je definisati CORS (Cross-origin resource sharing) konfiguraciju kako bi one mogle uspešno da im pristupaju zbog restrikcija koje web pretraživači postavljaju na pristup resursima sa drugog domena. Globalna konfiguracija CORS-a se postiže registracijom bean-a tipa `CorsConfigurationSource`. Moguće je kongiurisati CORS pravila i na nivou repozitorijuma i njegovih metoda korišćenjem `@CrossOrigin` annotacije.

## Spring Data REST HAL Browser

Spring Data REST HAL Browser je Spring modul koji pruža HAL pretraživač. S bozirom da Spring Data REST modul koristi HAL veoma je zgodno što postoji jednostavan način za uključivanje njegovor pretraživača u projekat. HAL pretraživač je veoma koristan u fazi razvoja jer programerima koji rade na backend-u pruža unificirani interfejs preko koga mogu pristupiti i isprobati REST API na kome rade. Ovaj pretraživač je veoma koristan i svima koji žele da koriste kreirani REST API jer ih na lak način upoznaje sa onim što API pruža.

HAL pretraživaču se pristupa preko istog URL-a na kome se nalazi REST API. Server će na osnovu Accept zaglavlja zahteva odlučiti da li treba da redirektuje korisnika na HAL pretraživač ili da ga propusti ka REST API-ju. Web pretraživači automatski šalju odgovarajuću vrednost Accept zaglavnja za HAL pretraživač.

Operacije koje HAL pretraživač nudi korisnicima su dovoljne za osnovnu upotrebu REST API-ja i pokrivaju pregled, kreiranje, modifikaciju i brisanje resursa.

### Prikaz izgleda HAL pretraživača.

Prilikom odlaska na koren REST API biće prezentovana stranica kao na slici 2.1. Ona po sadržaju podseća na JSON odgovor koji se dobija pri zahtevu uz pomoć komande CURL.

![Slika 2.1 - Koren](./images/hal-root.png){ width=100% }

Izgled stranice za kolekciju resursa je kao na sledećoj slici 2.2. Sa ove stranice je moguće pristupiti opcijama za kreiranje novog resursa.

![Slika 2.2 - Kolekcija users](./images/hal-users.png){ width=100% }

Stranica za pojedinačne resurse se nalazi na slici 2.3. Na njoj se nalaze i opcije za izmenu i brisanje resursa.

![Slika 2.3 - Resurs kolekcije users](./images/hal-user.png){ width=100% }

Forma za izmenu resursa je data na slici 2.4. HAL browser na osnovu informacija koje dobija od REST API ume da sam izgeneriše formu.

![Slika 2.4 - Forma za izmenu resursa kolekcije users](./images/hal-user-form.png){ width=100% }

