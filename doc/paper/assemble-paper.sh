rm -rf assembled
mkdir assembled

cat \
_Uvod.md \
_Tehnologije.md \
_Specifikacija.md \
_Implementacija.md \
_Pregled.md \
_Uputstvo.md \
_PrilogA.md \
_Zakljucak.md \
_Literatura.md \
_Biografija.md \
> "./assembled/Igor Gere e9969 Master Rad.md"

pandoc --latex-engine=xelatex \
-o "./assembled/Igor Gere e9969 Master Rad.odt" \
"./assembled/Igor Gere e9969 Master Rad.md"

pandoc \
-o "./assembled/Igor Gere e9969 Master Rad (RNI).odt" \
"./template/Template-naslovna+zadatak+sadrzaj.odt" \
"./assembled/Igor Gere e9969 Master Rad.odt" \
"./template/Template-dokumentacijska-informacija.odt"

# pandoc --latex-engine=xelatex \
# -o "./assembled/Igor Gere e9969 Master Rad (RNI).pdf" \
# "./assembled/Igor Gere e9969 Master Rad (RNI).odt"
