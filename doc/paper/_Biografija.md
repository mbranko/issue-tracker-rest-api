# Biografija

Igor Gere rođen je u Novom Sadu 11.06.1982. godine. Završio je osnovnu školu Svetozar Marković Toza, a potom i srednju elektrotehničku školu Mihajlo Pupin smer elektrotehničar elektronike. Godine 2001. upisuje Fakultet tehničkih nauka, odsek Računarstvo i automatika na kome bira usmerenje Računarske nauke i informatika. Položio je sve ispite predviđene planom i programom.

