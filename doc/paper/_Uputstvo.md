# Uputstvo za kretanje demonstrativne aplikacije

Demonstrativna aplikacija se sastoji od dve aplikacije, __frontend__ i __backend__ dela. S obzirom da su rađene uz pomoć različitih tehnologija one se pokreću na različite načine.

## Backend

Da bi se pokrenuo __backend__ server potrebno je da su zadovoljeni sledeći uslovi:
- Java 1.8 je instalirana (testirano je na verziji 1.8.0_181).
- Maven je instaliran (testirano je na verziji 3.5.2).
- TCP port 8090 je slobodan.

Komanda za pokretanje servera je data u listingu 6.1.

```bash
mvn clean spring-boot:run

```

> Listing 6.1

## Frontend

Da bi se pokrenuo __frontend__ server potrebno je da su zadovoljeni sledeći uslovi:
- Node.js je instaliran (probano je na verziji 8.12.0 LTS).
- TCP port 8080 je slobodan.

Komanda za pokretanje servera je data u listingu 6.2.

```bash
npm install
npm run serve -- -port 8080
```

> Listing 6.2

