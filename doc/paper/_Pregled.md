# Pregled izgleda aplikacije

Fokus ovog rada je __backend__ tehnologija, a da bi se ona što bolje izučila bilo je potrebno kreirati aplikaciju koja bi pristupala i koristila REST API izložen od strane __backend__-a. U tu svrhu kreirana je __frontend__ aplikacija u JavaScript-u i trenutno dosta popularnoj biblioteci Vue.js. Za stilizovanje HTML-a korišćena je biblioteka Bootstrap.

Pri pisanju __frontend__ aplikacije uočeni su i otklonjeni neki problemi koje je __backend__ posedovao. U nastavku sledi prikaz korisničkog interfejsa __frontend__ dela aplikacije.

Kao i sve aplikacije koje imaju korisnike i Issue Tracker poseduje autentifikacionu stranicu koja se prikazuje na početku. Izgled autentifikacione stranice dat je na slici 5.1.

![Slika 5.1](./images/it-login.png){ width=100% }


Posle uspešne autentifikacije korisnik dolazi na stranicu sa pregledom projekata. Na ovoj stranici je moguće preći na stranicu projekta, otići na stranicu za izmenu postojećeg projekta ili kreirati novi projekat. Izgled stranice dat je na slici 5.2.

![Slika 5.2](./images/it-home-projects.png){ width=100% }


Izgled stranice za kreiranje ili izmenu projekta. Na ovoj stranici se pojavljuje crvena ikonica sa kantom ako se radi o izmeni projekta, klikom na ikonicu projekat se briše iz baze. Izgled stranice dat je na slici 5.3.

![Slika 5.3](./images/it-project-form.png){ width=100% }


Pošto projekti mogu imati mnogo zadataka aplikacije podržava paginaciju koja je podešena tako da se klikom na "Load more..." dugme učitava novih šest zadataka (slika 5.4). Kada se učitaju svi postojeći zadaci projekta dugme nestaje. 

![Slika 5.4](./images/it-project.png){ width=100% }


Primer izlgeda projekta bez dugmeta "Load more..." jer su učitani svi zadaci dat je na slici 5.5.

![Slika 5.5](./images/it-project-loaded.png){ width=100% }


Na primeru forme sa slike 5.6 za dodavanje i izmenu zadatka kao i kod projekata za slučaj izmene postoji crvena ikonica kante koja služi kao dugme za brisanje zadatka.

![Slika 5.6](./images/it-issue-form.png){ width=100% }


Sve forme u aplikaciji poseduju validaciju. Na slici 5.7 je prikazano kako izgleda forma sa validacionim greškama koje su se pojavile posle slanja.

![Slika 5.7](./images/it-issue-form-validation.png){ width=100% }


Slika 5.8 prikazuje stranicu sa zadacima. Scaki zadatak pored svojih polja poseduje i komentare. Ikonica za izmenu komentara postoji samo na komentarima koje trenutni korisnik može po pravilima pristupa da izmeni. Podsećanja radi komentar može samo autor da menja, čak ni administratorima nije dozvoljeno da menjaju tuđe komentare.

![Slika 5.8](./images/it-issue.png){ width=100% }


Kao i kod liste projekta, na samom projektu sa zadacima tako i kod zadatka postoji paginacija komentara. I ovde kao i na ostalim delovima aplikacije učitava se u grupama po šest, slika 5.9.

![Slika 5.9](./images/it-issue-comments-pagination.png){ width=100% }


Komentari se mogu menjati na formi kao sa sledeće slike 5.10.

![Slika 5.10](./images/it-comment-form.png){ width=100% }


Aplikacija poseduju i deo za pregled i obradu korisnika dat na slici 5.11.

![Slika 5.11](./images/it-users.png){ width=100% }


Za razliku od svih ostalih entiteta na formi za izmenu korisnika ne postoji opcija za brisanje korisnika kao što se vidi na slici 5.12.

![Slika 5.12](./images/it-user-form.png){ width=100% }

