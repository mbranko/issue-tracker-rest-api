# ﻿ZADATAK RADA

Zadatak rada je prikaz razvoja demonstrativne web aplikacije korišćenjem Spring Data REST modula popularnog framework-a Spring. U okviru rada treba opisati i prikazati način korišćenja pomenutog modula i saznanja koja su nastala prilikom razvoja demonstrativne aplikacije. Ukazati na prednosti i mane korišćenja modula koji je u fokusu ovog rada.

U okviru rada kao demonstrativnu aplikaciju kreirati issue tracker (sistem za praćenje zadataka). Izbor funkcionalnosti treba da bude takav da prikaže tehnologiju u fokusu ovog rada bez cilja zadovoljavanja svih aspekata problema domena demonstrativne aplikacije.


